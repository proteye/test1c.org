<?php

class SitemapController extends Controller
{
    public function actionIndex()
    {
        $urls = array();
        
        // Вопросы 1С Профессионал 8.2
        $quests = Question::model()->findAll(array(
            'condition' => 'platform_id = 1',
        ));
        
        foreach ($quests as $quest) {
            $urls[] = $this->createUrl('training/' . $quest->id);
        }
        
        $this->renderPartial('index', array(
            'host' => Yii::app()->request->hostInfo,
            'urls' => $urls,
        ));
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}