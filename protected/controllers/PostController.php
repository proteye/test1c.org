<?php

class PostController extends Controller
{
    public function actionIndex()
    {
        $model = Post::model()->findAll();
        $this->render('index', array('model' => $model));
    }

    public function actionView($id, $url = '')
    {
        $model = Post::model()->findByPk($id);
        if (!$model)
            throw new CHttpException(404, 'Указанная страница не найдена');
        elseif ($url != '' && $model->url != $url)
            throw new CHttpException(404, 'Указанная страница не найдена');
        
        if ($url === '') {
            $url = $model->url;
            $this->redirect('/post/'.$id.'/'.$url, true, 301);
        }
        
        // SEO
        $this->pageTitle = $model->title;
        $this->description = $model->description;
        $this->keywords = $model->keywords;
        
        // Генерация шаблона и вывод на экран
        $this->render('view', array('model' => $model));
    }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}