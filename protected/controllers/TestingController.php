<?php

class TestingController extends Controller
{
    public $layout='//layouts/column2';
    
    public function filters() {
        return array(
            'ajaxOnly + checkanswer', // action'ы вызываемые только ajax'ом.
        );
    }
    
    /*
     * Главная страница Тестирования.
     */
    public function actionIndex()
    {
        // SEO
        $this->pageTitle = 'Тестирование по 1С Предприятие Платформа 8.2';
        $this->description = 'Пробное тестирование по 1С Предприятие Платформа 8.2';
        $this->keywords = 'тестирование пробное 1с профессионал платформа подготовка';

        $this->render('index');
    }

    public function actionGo()
    {
        // С какого Раздела начать тест - $_POST['tnq']
        $post_tnq = (int)Yii::app()->request->getPost('tnq', 1);
        
        // SEO
        $this->pageTitle = 'Начало тестирования по 1С Профессионал Платформа 8.2';
        $this->description = 'Начало пробного тестирования по 1С Профессионал Платформа 8.2';
        $this->keywords = 'начало тестирование 1с профессионал платформа';
        
        // Обнуляем массив сессии testing
        $session=new CHttpSession;
        $session->open();
        unset($session['testing']);
        
        /*
         * jQuery скрипт, который ajax'ом отправляет вариант ответа пользователя по адресе /testing/checkanswer.
         * Также отправляется id и num_quest текущего вопроса.
         */
        Yii::app()->clientScript->registerScript(
                'checkAnswer',
                'jQuery("body").on("click", "input[name=answer]", function() {
                    var answer_id = $(this).attr("alt");
                    var quest_id = $("input[name=quest_id]").val();
                    var num_quest = $("input[name=num_quest]").val();
                    jQuery.ajax({
                        data:{"answer_id": answer_id,"num_quest": num_quest, "quest_id": quest_id},
                        url:"/testing/checkanswer",
                        cache:false,
                        success:function(html){
                            jQuery(".large-9").html(html)
                            }
                        });
                    return false;
                    });',
                CClientScript::POS_READY);
        
        $pid = 1; // platform_id - id платформы, по которой проводится тест
        $tnq = ($post_tnq > 1 && $post_tnq < 15) ? $post_tnq : 1; // top num_quest - верхняя цифра номера вопроса
        $bnq = $tnq + 1; // bottom num_quest - нижняя цифра номера вопроса
        $session['tnq'] = $tnq;
        
        $model = Question::model()->with('answers')->with('image')->findAll(
                'platform_id = :pid AND num_quest >= :tnq AND num_quest < :bnq',
                array(':pid' => $pid, ':tnq' => $tnq, ':bnq' => $bnq));
        
        if (!$model) {
            throw new CHttpException(400,'Некорректный запрос.');
        }
        
        $quests = count($model); // количество вопросов в разделе
        $curQuest = rand(0, $quests-1); // текущий случайный вопрос
        
        $this->render('go', array('model' => $model[$curQuest]));
    }
    
    public function actionCheckAnswer()
    {
        $quest_id = (int)Yii::app()->request->getQuery('quest_id', 0); // текущий id вопроса
        $num_quest = Yii::app()->request->getQuery('num_quest', 0);
        $answer_id = (int)Yii::app()->request->getQuery('answer_id', 0); // id ответа пользователя
        $true_answ = Answer::model()->findByPk($answer_id)->true_answ; // ответ был правильным (1) или нет (0)
        
        // Открываем сессию
        $session=new CHttpSession;
        $session->open();
        
        // К массиву testing добавляем очередной массив значений вопроса и ответа пользователя
        $testing = array();
        if (isset($session['testing']))
            $testing = $session['testing'];
        $testing[] = array('quest_id' => $quest_id, 'num_quest' => $num_quest, 'answer_id' => $answer_id, 'true_answ' => $true_answ);
        $session['testing'] = $testing;
        
        // print_r($session['testing']);
        
        $pid = 1; // platform_id - id платформы, по которой проводится тест        
        $tnq = (int)$num_quest + 1; // top num_quest - верхняя цифра номера вопроса
        $bnq = $tnq + 1; // bottom num_quest - нижняя цифра номера вопроса
        
        $model = Question::model()->with('answers')->with('image')->findAll(
                'platform_id = :pid AND num_quest >= :tnq AND num_quest < :bnq',
                array(':pid' => $pid, ':tnq' => $tnq, ':bnq' => $bnq));
        
        // Если вопросов больше нет, то ТЕСТ считается завершенным
        if (!$model) {
            $result = array();
            $f_answ = array();
            $result['count_all'] = count($testing);
            $result['count_true'] = 0;
            foreach ($testing as $t) {
                ($t['true_answ']) ? $result['count_true']++ : null;
                (!$t['true_answ']) ? $f_answ[$t['quest_id']] = $t['num_quest'] : null;
            }
            $result['false_answ'] = $f_answ;
            $result['success'] = (($result['count_true']/$result['count_all']) > 0.85) ? 1 : 0;
            
            $this->renderPartial('result', array('result' => $result, 'tnq' => $session['tnq']));
            Yii::app()->end();
        }
        
        $quests = count($model); // количество вопросов в разделе
        $curQuest = rand(0, $quests-1); // текущий случайный вопрос
        
        $this->renderPartial('go', array('model' => $model[$curQuest]));
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
            // return the filter configuration for this controller, e.g.:
            return array(
                    'inlineFilterName',
                    array(
                            'class'=>'path.to.FilterClass',
                            'propertyName'=>'propertyValue',
                    ),
            );
    }

    public function actions()
    {
            // return external action classes, e.g.:
            return array(
                    'action1'=>'path.to.ActionClass',
                    'action2'=>array(
                            'class'=>'path.to.AnotherActionClass',
                            'propertyName'=>'propertyValue',
                    ),
            );
    }
    */
}