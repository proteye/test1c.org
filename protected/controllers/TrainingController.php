<?php

class TrainingController extends Controller
{
    public $layout='//layouts/column2';
    
    public function filters() {
        return array(
            'ajaxOnly + checkanswer, searchquest', // action'ы вызываемые только ajax'ом.
        );
    }
    
    /*
     * Главная страница Тренинга.
     */
    public function actionIndex($id = 1)
    {
        // SEO
        $this->pageTitle = 'Тренинг по 1С Профессионал Платформа 8.2';
        $this->description = 'Тренинг по подготовке к экзамену по 1С Профессионал Платформа 8.2';
        $this->keywords = 'тренинг 1с предприятие профессионал платформа экзамен';
        
        // Проверяем cookie на наличие id последнего пройденного вопроса.
        if (CHttpRequest::getUrlReferrer() && !strpos(CHttpRequest::getUrlReferrer(), 'training') && !strpos(CHttpRequest::getUrlReferrer(), 'yandex') && !strpos(CHttpRequest::getUrlReferrer(), 'google') && !strpos(CHttpRequest::getUrlReferrer(), 'testing/go') && isset(Yii::app()->request->cookies['question_id'])) {
            $id = (string)Yii::app()->request->cookies['question_id'];
            if (!isset(Yii::app()->session['redirect'])) {
                Yii::app()->session['redirect'] = 1;
                Yii::app()->request->redirect("/training/$id");
            }
        }
        unset(Yii::app()->session['redirect']);
        $model = Question::model()->with('answers')->with('image')->findByPk((int)$id, array(
            'order' => 'answers.num_answ ASC',
        ));
        $arr_id = array();
        
        if($model===null)
            throw new CHttpException(404,'Запрошенная вами страница не найдена.');
        
        // Регистрация клиентского скрипта для маскирования input.
        // Yii::app()->clientScript->registerScriptFile('/js/jquery.maskedinput.min.js');
        
        /*
         * Функция jQuery, которая принимает ajax ответ от checkAnswer, 
         * выводит результат в #result и подсвечивает правильный ответ.
         */
        Yii::app()->clientScript->registerScript(
                'checkAnswer',
                'function checkAnswer(html) {
                    if (html.tr == 1) {
                        var data = "<span id=\"true\">Правильно</span>";
                        $("input[value="+html.transw+"]").addClass("success").parent().next("div").addClass("success");
                    }
                    else {
                        var data = "<span id=\"false\">Не верно!</span>";
                        $("input[value="+html.transw+"]").addClass("active").parent().next("div").addClass("success");
                    }
                    $("#result").html(data);
                    if (html.next != false) {
                        setTimeout(function() {
                            document.location.href = "/training/" + html.next;
                        }, 1500);
                    }
                    else {
                    // Вывод запроса после завершения тренинга.
                        setTimeout(function() {
                            $("<div id=\"backgr\"></div>").appendTo("body");
                            $("<div class=\"end_training\"><h4>Тренинг завершен</h4>Хотите начать заново?<br/><br/><a class=\"button small success\" id=\"training_yes\">Да</a><a class=\"button small\" id=\"training_no\">Нет</a></div>").appendTo(".large-9");
                        }, 1500);                        
                    }
                }',
                CClientScript::POS_READY);
        // Скрипт, который обрабатывает ответ пользователя на вопрос "Хотите начать заново тренинг?"
        Yii::app()->clientScript->registerScript(
                'training_restart',
                '$("#training_yes").live("click", function() {
                    document.location.href = "/training/1";
                    return false;
                    });
                $("#training_no").live("click", function() {
                    document.location.href = "/";
                    return false;
                    });',
                CClientScript::POS_READY);
        // Скрипт, который обрабатывает переход по номер вопроса.
        Yii::app()->clientScript->registerScript(
                'searchquest',
                '$("#searchbut").click(function() {
                    jQuery.get("/training/searchquest", { questnum: $("input[name=questnum]").val() }, function(data) {
                        if (data)
                            document.location.href = "/training/" + data;
                    });
                    return false;
                });',
                CClientScript::POS_READY);
        
        // Скрипт, который обрабатывает переход по номер вопроса по нажатию на Enter.
        Yii::app()->clientScript->registerScript(
                'searchquest_enter',
                '$("#questnum").keypress(function(e) {
                    if (e.keyCode == 13) {
                    jQuery.get("/training/searchquest", { questnum: $("input[name=questnum]").val() }, function(data) {
                        if (data)
                            document.location.href = "/training/" + data;
                    });
                    }
                });',
                CClientScript::POS_READY);
        // Скрипт, который обрабатывает клавишы ВЛЕВО и ВПРАВО для навигации по вопросам.
        Yii::app()->clientScript->registerScript(
                'searchquest_lt_rt',
                '$(document).keypress(function(e) {
                    var id = '.$id.';
                    switch (e.keyCode) {
                        // Влево
                        case 37:
                            id = (id > 1) ? id-1 : false;
                            if (id)
                                document.location.href = "/training/" + id;
                            break;
                        // Вправо
                        case 39:
                            id = (id < 948) ? id+1 : false;
                            if (id)
                                document.location.href = "/training/" + id;
                            break;
                    }
                });',
                CClientScript::POS_READY);
        
        // Скрипт, который обрабатывает переход по номер вопроса.
        /*
        Yii::app()->clientScript->registerScript(
                'maskedinput',
                'jQuery("input[name=questnum]").mask("99.99");',
                CClientScript::POS_READY);
        */
        
        // Записываем в массив id следующего и предыдущего вопроса для передачи в вид.
        $arr_id['prev'] = ($id > 1) ? $id-1 : false;
        $arr_id['next'] = ($id < 948) ? $id+1 : false;
        
        // Вывод на экран.
        $this->render('index', array(
            'model' => $model,
            'arr_id' => $arr_id,
        ));
    }
    
    /*
     * Проверка ответа пользователя AJAX.
     */
    public function actionCheckAnswer($id)
    {
        // $model = Question::model()->with('answers')->findByPk((int)$id);
        /*
        if($model===null)
            return false;
        */
        if (isset($_GET['answer'])) {
            $answer_id = (int)$_GET['answer'];
            $model = Answer::model()->findByPk($answer_id);
            if ($model->true_answ) {
                $tr = 1;
            } else {
                $model = Answer::model()->find(array(
                    'condition'=>'question_id=:q_id AND true_answ=:t_answ',
                    'params'=>array(':q_id'=>(int)$id, ':t_answ'=>1),
                ));
                $tr = 0;
            }
            
            // Следующий id вопроса или конец тренинга (false). Всего 948 вопросов.
            $next = ($id < 948) ? $id+1 : false;
            $question_id = $next ? $next : 1; // номер вопроса, который будет записан в cookie.
            
            // Запись id последнего вопроса в Cookie.
            Yii::app()->request->cookies['question_id'] = new CHttpCookie('question_id', $question_id, array(
                'expire' => time()+86400*30, // cookie устанавливаем на 30 дней.
            ));
            
            echo CJSON::encode(array('tr' => $tr, 'next' => $next, 'transw' => $model->num_answ));
        }
        Yii::app()->end();
    }
    
    /*
     * Проверка ответа пользователя AJAX.
     */
    public function actionSearchQuest($questnum)
    {
        if (isset($questnum) && preg_match('/[0-9]+\.[0-9]+/', $questnum)) {
            $model = Question::model()->find('num_quest=:nq', array(':nq' => $questnum));
            echo $model->id;
        }
        else {
            echo false;
        }
        Yii::app()->end();
    }
    
    /*
     * Функция возврата правильного ответа из списка ответов.
     */
    private function trueAnswer($answers)
    {
        foreach ($answers as $answer) {
            if ($answer->true_answ == 1)
                return $answer->num_answ; // возвращаем правильный ответ.
        }
        
        return false; // если правильный ответ не найден в базе, то false.
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}