<?php

/**
 * This is the model class for table "{{post}}".
 *
 * The followings are the available columns in table '{{post}}':
 * @property integer $id
 * @property string $url
 * @property string $title
 * @property string $text
 * @property string $announce
 * @property string $image
 * @property string $keywords
 * @property string $description
 * @property integer $created
 * @property integer $updated
 * @property integer $status
 */
class Post extends CActiveRecord
{
        const YES = 1;
        const NO = 0;
        const UPLOAD_FOLDER = '/upload/images/';
        
        public function init()
        {
            parent::init();
            // Устанавливаем "Отображать?" при создании Категории по умолчанию "ДА".
            if ($this->isNewRecord) {
                $this->status = self::YES;
            }
        }
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{post}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('url, title, text', 'required'),
			array('created, updated, status', 'numerical', 'integerOnly'=>true),
			array('url, title, keywords, description', 'length', 'max'=>255),
                        array('image', 'file', 'types' => 'jpg, gif, png', 'allowEmpty' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, url, title, text, announce, image, keywords, description, created, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'url' => 'Url',
			'title' => 'Title',
			'text' => 'Text',
			'announce' => 'Announce',
                        'image' => 'Image',
			'keywords' => 'Keywords',
			'description' => 'Description',
			'created' => 'Created',
			'updated' => 'Updated',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('announce',$this->announce,true);
                $criteria->compare('image',$this->image,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('created',$this->created);
		$criteria->compare('updated',$this->updated);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
       // Действия перед сохранением
        protected function beforeSave()
        {
            if (!parent::beforeSave())
                return false;
            
            // Формируем Дату создания объекта.
            if ($this->isNewRecord) {
                $this->created = time();
                $this->updated = time();
            }
            else {
                $this->updated = time();
            }
            
            // Сохраняем файл изображения на диске
            $image = CUploadedFile::getInstance($this, 'image');
            if (isset($image))
            {
                $imgType = $image->extensionName;
                $imgName = explode('.', $image);
                $imgName = $imgName[0];
                $imageUrl = $this->getFolder() . $image;
                $k = 1;
                while (is_file($imageUrl)) {
                    $img_name = $imgName . '_' . $k++ . '.' . $imgType;
                    $imageUrl = $this->getFolder() . $img_name;
                }
                $this->deleteImage();
                $this->image = $img_name;
                $image->saveAs($imageUrl);
            }

            return true;
        }
        
         // Действия после сохранения
        protected function afterSave() {
            parent::afterSave();
            
            if ($this->isNewRecord) {
                // Yii::app()->rpcManager->pingPage($this->getUrl()); // weblog.ping - отправляем ping оповещение на yandex и google о новой статье
            }
        }
        
        private $_url; // url статьи
        
        // Получение url статьи
        public function getUrl(){
            if ($this->_url === null)
                $this->_url = Yii::app()->createUrl('post/view', array('id' => $this->id, 'url' => $this->url));
            return $this->_url;
        }
        
        // Получаем папку для сохранения изображений
        public function getFolder()
        {
            $folder = Yii::getPathOfAlias('webroot') . self::UPLOAD_FOLDER;
            if (is_dir($folder) == false)
                mkdir($folder, 0755, true);
            return $folder;
        }
        
        // Получаем папку для сохранения изображений
        public function deleteImage()
        {
            $imgPath = $this->getFolder() . $this->image;
            if (is_file($imgPath))
                unlink($imgPath);
        }
}