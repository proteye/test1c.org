<?php
/* @var $this TestingController */

$this->breadcrumbs=array(
	'Тестирование',
);
?>
<h1>Тестирование</h1>
<p>
    Добавлена возможность выбирать Раздел, с которого хотите начать Тестирование.
</p>
<p class="text-right"><i>30 мая 2014 г.</i></p>
<p>
    В модуле "Тестирование" была выявлена ошибка: всегда отправлялся 1-ый ответ в списке, 
    независимо от выбора пользователя. Но благодаря письмам посетителей сайта ошибка была 
    выявлена и устранена.<br/>
    Также, неправильно отвеченные, вопросы в результатах теста стали кликабельны.
</p>
<p class="text-right"><i>2 апреля 2014 г.</i></p>
<h3 class="text-center">Удачного вам тестирования!</h3><br/>
<?= CHtml::beginForm('/testing/go', 'post'); ?>
<div class="row">
    <div class="large-6 medium-6 large-centered medium-centered columns">
        <select name="tnq" title="Выберите с какого раздела хотите начать Тест">
            <option value="1">Полный тест</option>
            <?php for ($i = 2; $i <= 14; $i++): ?>
            <option value="<?= $i; ?>">Раздел <?= $i; ?></option>
            <?php endfor; ?>
        </select>
    </div>
</div>
<div class="row">
    <div class="large-6 medium-6 large-centered medium-centered columns text-right">
        <?= CHtml::button('Начать тестирование', array('type' => 'submit', 'class' => 'button success')); ?>
    </div>
</div>
<?=CHtml::endForm(); ?>