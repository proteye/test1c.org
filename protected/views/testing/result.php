<?php
/* @var $this TestingController */

$this->breadcrumbs='';
?>
<h1>Результаты тестирования</h1>
<?= $result['success'] ? '<div id="text_green"><h2>Поздравляем!<br/> Вы успешно сдали экзамен.</h2></div>' : '<div id="text_red"><h2>К сожалению, вы не сдали экзамен.</h2></div> <h3>Уделите еще время для подготовки.</h3>'; ?>
<table>
    <tr>
        <td>Количество вопросов:</td>
        <td><?= $result['count_all']; ?></td>
    </tr>
    <tr>
        <td>Правильных ответов:</td>
        <td><?= $result['count_true']; ?></td>
    </tr>
    <tr>
        <td>Ошибки допущены в вопросах:</td>
        <td>
            <? $i = 0;
            foreach($result['false_answ'] as $key=>$val):
                $i++;
                echo CHtml::link($val, array('/training/'.$key), array('rel' => 'nofollow', 'target' => '_blank'));
                if($i < count($result['false_answ']))
                    echo ', ';
            endforeach; ?>
        </td>
    </tr>
</table>
<?= CHtml::beginForm('/testing/go', 'post'); ?>
<div class="row">
    <div class="large-6 medium-6 columns">
        <select name="tnq" title="Выберите с какого раздела хотите начать Тест">
            <option value="1">Полный тест</option>
            <?php for ($i = 2; $i <= 14; $i++): ?>
            <option value="<?= $i; ?>" <?= ($i == $tnq) ? 'selected' : null; ?> >Раздел <?= $i; ?></option>
            <?php endfor; ?>
        </select>
    </div>
</div>
<div class="row">
    <div class="large-6 medium-6 columns text-right">
        <?= CHtml::button('Пройти тест заново', array('type' => 'submit', 'class' => 'button')); ?>
    </div>
</div>
<?=CHtml::endForm(); ?>