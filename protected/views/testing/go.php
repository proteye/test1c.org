<?php
/* @var $this TrainingController */

$this->breadcrumbs='';
?>
<div class="question">
<h1><?= $model->num_quest . '. ' . $model->text_quest; ?></h1>
<?php if(isset($model->image)): ?>
<img src="/images/<?= $model->image->url ?>" alt="<?= $model->image->name ?>" width="600px"/>
<?php endif; ?>
<div id="result"></div>
    <ul class="answers">
        <? foreach ($model->answers as $answer): ?>
        <li>
            <div class="row collapse">
                <div class="small-1 columns">
                    <?= CHtml::button($answer->num_answ, array('name' => 'answer', 'alt' => $answer->id)); ?>
                </div>
                <div class="small-11 columns">
                    <?= $answer->text_answ; ?>
                </div>
            </div>
        </li>
        <? endforeach; ?>
    </ul>
</div>
<?= CHtml::hiddenField('quest_id', $model->id); ?>
<?= CHtml::hiddenField('num_quest', $model->num_quest); ?>