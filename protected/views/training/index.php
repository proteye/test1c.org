<?php
/* @var $this TrainingController */

$this->breadcrumbs=array(
	'Тренинг',
);
?>
<div class="question">
<span style="font-size: 10pt; color: #0079A1; background: #F4F4F4; border: 1px solid #DDDDDD; padding: 3px; border-radius: 3px;">
    Для навигации возможно использование клавиш <i>Влево</i> и <i>Вправо</i>.
</span>
<h1><?= $model->num_quest . '. ' . $model->text_quest; ?></h1>
<?php if(isset($model->image)): ?>
<img src="/images/<?= $model->image->url ?>" alt="<?= $model->image->name ?>" width="600px"/>
<?php endif; ?>
<div id="result"></div>
    <ul class="answers">
        <? foreach ($model->answers as $answer): ?>
        <li>
            <div class="row collapse">
                <div class="small-1 columns">
                    <?php echo CHtml::ajaxButton($answer->num_answ, array('training/checkAnswer', 'id' => $model->id), array(
                        'update' => '#result',
                        'dataType' => 'json',
                        'data' => array(
                            'answer' => $answer->id,
                        ),
                        'success' => 'checkAnswer',
                    )); ?>
                </div>
                <div class="small-11 columns">
                    <?= $answer->text_answ; ?>
                </div>
            </div>
        </li>
        <? endforeach; ?>
    </ul>
</div>
<div class="nav-quest">
<div class="row collapse">
    <div class="large-4 columns">
        <div class="row collapse">
            <!--noindex-->
            <div class="small-6 columns">
                <? if ($arr_id['prev']): ?>
                <?= CHtml::link('Предыдущий', array($arr_id['prev']), array('class' => 'button tiny', 'rel' => 'nofollow')) ?>
                <? else: ?>
                <div class="button tiny disabled">Предыдущий</div>
                <? endif; ?>
            </div>
            <!--/noindex-->
            <div class="small-6 columns">
                <? if ($arr_id['next']): ?>
                <?= CHtml::link('Следующий', array($arr_id['next']), array('class' => 'button tiny success')) ?>
                <? else: ?>
                <div class="button tiny success disabled">Следующий</div>
                <? endif; ?>
            </div>
        </div>
    </div>
    <!--noindex-->
    <div class="large-6 columns">
        <div class="row collapse">
            <div class="small-8 columns">
                <?= CHtml::textField('questnum', '', array('placeholder' => 'Вопрос №. Например: 5.16', 'title' => 'Для перехода нажмите Enter')); ?>
            </div>
            <div class="small-4 columns">
                <?= CHtml::link('Перейти', '#', array('class' => 'button postfix', 'id' => 'searchbut', 'rel' => 'nofollow')); ?>
            </div>
        </div>
    </div>
    <!--/noindex-->
</div>
</div>