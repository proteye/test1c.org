<?php
/* @var $this PostController */

$this->breadcrumbs=array(
	'Статьи' => array('post/index'),
        $model->title,
);
?>
<h1><?= $model->title; ?></h1>

<?= $model->text; ?>

<!--noindex-->
<?= CHtml::link('Вернуться ко всем статьям', array('post/index'), array('class' => 'right', 'rel' => 'nofollow')); ?>
<!--/noindex-->