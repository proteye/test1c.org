<?php
/* @var $this PostController */

$this->breadcrumbs=array(
	'Статьи',
);
?>
<h1>Статьи по 1С</h1>
<hr/>

<? if ($model):
foreach ($model as $post): ?>
    <?= CHtml::link($post->title, '/post/'.$post->id.'/'.$post->url) ?>
    <hr/>
<? endforeach;
else:
    echo 'Нет ни одной статьи.';
endif;
?>