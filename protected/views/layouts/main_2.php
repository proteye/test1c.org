<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="Description" content="<?php echo CHtml::encode($this->description); ?>"/>
    <meta name="Keywords" content="<?php echo CHtml::encode($this->keywords); ?>"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- foundation CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/foundation.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />    

    <!-- foundation framework JS -->
    <!-- <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/modernizr.js"></script> -->
    <!-- <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/foundation.min.js"></script> -->
</head>

<body>

<!--noindex-->
<div class="row">
    <div class="large-12 columns">
	<div class="loginmenu right">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Войти', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest, 'linkOptions'=>array('rel'=>'nofollow')),
				array('label'=>'Регистрация', 'url'=>array('/site/register'), 'visible'=>Yii::app()->user->isGuest, 'linkOptions'=>array('rel'=>'nofollow')),
				array('label'=>'Выйти ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest, 'linkOptions'=>array('rel'=>'nofollow'))
			),
		)); ?>
	</div>
    </div>
</div>
<!--/noindex-->
<div class="row">
    <div class="large-12 columns">
        <nav class="top-bar" data-topbar>
            <ul class="title-area">
                <li class="name">
                    <?php if(Yii::app()->request->pathInfo == ''): ?>
                        <div>1С Тренинг</div>
                    <?php else: ?>
                        <a href="/" rel="nofollow">1С Тренинг</a>
                    <?php endif; ?>
                </li>
            </ul>
            <section class="top-bar-section">
                <ul class="right">
                    <li class="divider"></li>
                    <li <?= strstr(Yii::app()->request->pathInfo, 'training') ? 'class="active"' : ''; ?>><?= CHtml::link('Тренинг', array('/training/index'), array('rel' => strstr(Yii::app()->request->pathInfo, 'training') ? 'nofollow' : '')); ?></li>
                    <li class="divider"></li>
                    <li <?= strstr(Yii::app()->request->pathInfo, 'testing') ? 'class="active"' : ''; ?>><?= CHtml::link('Тестирование', array('/testing/index'), array('rel' => strstr(Yii::app()->request->pathInfo, 'testing') ? 'nofollow' : '')); ?></li>
                    <li class="divider"></li>
                    <li <?= strstr(Yii::app()->request->pathInfo, 'reviews') ? 'class="active"' : ''; ?>><?= CHtml::link('Отзывы', array('/site/reviews'), array('rel' => strstr(Yii::app()->request->pathInfo, 'reviews') ? 'nofollow' : '')); ?></li>
                </ul>
            </section>
        </nav>
    </div>
</div>

<!--noindex-->
<div class="row">
    <div class="large-12 columns">
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links' => $this->breadcrumbs,
                        'homeLink' => '<li>' . CHtml::link('Главная', array('site/index'), array('rel' => 'nofollow')) . '</li>',
                        'tagName' => 'ul',
                        'separator' => '',
                        'activeLinkTemplate' => '<li><a href="{url}" rel="nofollow">{label}</a></li>',
                        'inactiveLinkTemplate' => '<li class="current">{label}</li>',
		)); ?><!-- breadcrumbs -->
	<?php endif?>
        <hr/>
    </div>
</div>
<!--/noindex-->

<div id="content">
<?php echo $content; ?>
</div>

<div class="row">
    <div class="large-12 columns">
        <div class="footer text-right">Development by Yii Framework. Все права защищены. 2011-<?= date('Y'); ?>. <!--noindex--><a href="mailto:admin@test1c.org?subject=Feedback%20from%20-%20test1c.org" rel="nofollow">@admin</a><!--/noindex--></div>
    </div>
</div>

<!--noindex-->
<br/>
<div class="row">
    <div class="large-12 columns text-right">
    
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">//<![CDATA[
var _tmr = _tmr || [];
_tmr.push({id: "2458451", type: "pageView", start: (new Date()).getTime()});
(function (d, w) {
   var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
   ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
   var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
   if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window);
//]]></script><noscript><div style="position:absolute;left:-10000px;">
<img src="//top-fwz1.mail.ru/counter?id=2458451;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
</div></noscript>
<!-- //Rating@Mail.ru counter -->

<!-- Rating@Mail.ru logo -->
<a href="http://top.mail.ru/jump?from=2458451">
<img src="//top-fwz1.mail.ru/counter?id=2458451;t=281;l=1" 
style="border:0;" height="31" width="38" alt="Рейтинг@Mail.ru" /></a>
<!-- //Rating@Mail.ru logo -->

<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter18753502 = new Ya.Metrika({id:18753502, clickmap:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/18753502" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->

<!--LiveInternet counter--><script type="text/javascript">document.write("<a href='http://www.liveinternet.ru/click' target=_blank><img src='//counter.yadro.ru/hit?t44.5;r" + escape(document.referrer) + ((typeof(screen)=="undefined")?"":";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?screen.colorDepth:screen.pixelDepth)) + ";u" + escape(document.URL) + ";" + Math.random() + "' border=0 width=31 height=31 alt='' title='LiveInternet'><\/a>")</script><!--/LiveInternet-->

    </div>
</div>
<!--/noindex-->

</body>

</html>