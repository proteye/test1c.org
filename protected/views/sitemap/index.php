<?php /* @var $this SitemapController */ ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>http://test1c.org/</loc>
        <lastmod>2014-01-21</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.8</priority>
    </url>
    <url>
        <loc>http://test1c.org/training</loc>
        <changefreq>monthly</changefreq>
        <priority>0.2</priority>
    </url>
    <url>
        <loc>http://test1c.org/testing</loc>
        <changefreq>monthly</changefreq>
        <priority>0.2</priority>
    </url>
    <url>
        <loc>http://test1c.org/reviews</loc>
        <changefreq>weekly</changefreq>
        <priority>0.5</priority>
    </url>
<?php
foreach ($urls as $url) {
    echo '<url>
        <loc>' . $host . $url . '</loc>
        <changefreq>yearly</changefreq>
        <priority>0.5</priority>
    </url>';
}
echo PHP_EOL;
?>
</urlset>