<?php

Yii::import('application.vendor.IXR_Library', true);
 
class DRPCManager extends CApplicationComponent
{
    public $pingEnable = true;
    public $pingServers = array();
 
    public function pingPage($pageURL)
    {
        if($this->pingEnable)
        {
            if (!$pageURL)
                return;
 
            $siteName = Yii::app()->name;
            $siteHost = Yii::app()->request->getHostInfo();
            $fullPageUrl = $siteHost . $pageURL;
 
            foreach ($this->pingServers as $serverUrl)
            {
                if (preg_match('|(?P<host>\w+://[\w\d\._-]+)(/?P<uri>.*)|i', $serverUrl, $matches))
                {
                    $client = new IXR_Client($matches['host'], $matches['uri']);
                    if (!$client->query('weblogUpdates.ping', array($siteName, $siteHost, $fullPageUrl)))
                        Yii::log('Ping error for ' . $serverUrl, CLogger::LEVEL_WARNING);
                }
            }
        } else
            Yii::log('Emulation of ping for ' . $fullPageUrl);
    }
}